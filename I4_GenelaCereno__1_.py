#!/usr/bin/env python
# coding: utf-8

# In[5]:


# install requests package to pull data (only need to run this once)
get_ipython().system('pip3 install requests')


# In[27]:


# install packages to pull data
import requests
import json
from bs4 import BeautifulSoup
import csv
import pandas as pd


# In[7]:


# 1. NYT API code

#api key 
api_key = "j9SifAcsBQ8ggZsoyGb7vHmWUGGAWo7t"

#url
url = "https://api.nytimes.com/svc/search/v2/articlesearch.json"

#query parameters
query_params = query_params = {
    "q": "seattle protests",
    "begin_date": "20220101",
    "end_date": "20221231",
    "page": 0,
    "api-key": api_key
}


# In[ ]:


#To get started with accessing an API Key from New York Times, follow this link: https://developer.nytimes.com/get-started
#To customize your query parameters to pull the necessary API data, follow this link: https://developer.nytimes.com/apis


# In[12]:


# pull API data
response = requests.get(url, params=query_params)
response_json = json.loads(response.text)

# print responses
if response.status_code == 200:
    data = response.json()
    articles = data["response"]["docs"]
    for article in response_json["response"]["docs"]:
        headline = article["headline"]["main"]
        snippet = article["snippet"]
        print(headline + "\n" + snippet + "\n")
else:
    print("Error: ", response.status_code)


# In[ ]:


#1st Methodology Pros:
# - API key is quick and easy to access
# - Very simple and easy to follow guide on how to acquire API key

#1st Methodology Cons:
# - No code provided on how to print/collect certain aspects from data collected
# - No template provided on how to pull data from the API


# In[22]:


# 2. Webscraping CauseIQ

#url to be webscraped
url2 = "https://www.causeiq.com/directory/civil-rights-and-social-justice-organizations-list/seattle-tacoma-bellevue-wa-metro/"

#variable where data is getting pulled from
response = requests.get(url2)

#if else function for scraping through the url and pulling the names of the organizations and placing them into a csv file
#this function also takes into account errors outside of 200 that may arise
if response.status_code == 200:
    soup = BeautifulSoup(response.content, 'html.parser')
    organizations = soup.find('div', class_='directory-search').find_all('h3')
    data = []
    for org in organizations:
        title = org.text.strip()
        data.append([title])
    with open('organization_titles.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Title'])
        for row in data:
            writer.writerow(row)
else:
    print("Error: ", response.status_code)


# In[24]:


# with loop function takes each row of data of the title of organizations into an array (or list?)
with open('organization_titles.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    headers = next(reader)  # skip the header row
    data = []
    for row in reader:
        data.append(row)

# print out headers of dataset and the data list/array created
print(headers)
print(data)


# In[ ]:


#Link to site being webscraped here: https://www.causeiq.com/directory/civil-rights-and-social-justice-organizations-list/seattle-tacoma-bellevue-wa-metro/
#Webscraping code taken from ChatGPT, some html class names were changed after Google Inspecting the website to make sure the proper headers were scraped.


# In[ ]:


#2nd Methodology Pros:
# - Able to create data from any site so long as the code has the ability to be inspected!
# - Not a lot of steps required to webscrape

#2nd Methodology Cons:
# - The site being webscraped doesn't provide an open access API or anything
# - Instructions on webscraping are sort of self-taught/searched for, there's not really a template offered on how to webscrape websites for data
# - The method can be difficult, especially when HTML elements aren't named with classes


# In[30]:


# 3. CSV File download from Kaggle

#read/open csv file into jupyter notebook using pandas
df = pd.read_csv("seattle-cultural-space-inventory.csv")

#print out/display first 5 rows of dataset
df[0:5]


# In[ ]:


#3rd Methodology Pros:
# - Low code method
# - Very easy to find ways to open and access data of CSV files online

#3rd Methodology Cons:
# - There are a lot of different ways to open/read downloadable files (by functions or simple package functions) and it can be hard to find out which one works best for you

